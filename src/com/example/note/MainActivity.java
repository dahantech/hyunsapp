package com.example.note;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

//버튼 클릭 이벤트처리를 위해 OnClickListener 인터페이스 상속
public class MainActivity extends Activity implements OnClickListener {
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		// xml파일에 있는 버튼을 id값으로 불러와서 사용 , 버튼 텍스트 변경
		Button bt1 = (Button) findViewById(R.id.button1);
		Button bt2 = (Button) findViewById(R.id.button2);
		Button bt3 = (Button) findViewById(R.id.button3);
		Button bt4 = (Button) findViewById(R.id.button4);
		Button bt5 = (Button) findViewById(R.id.button5);
		

		// 버튼 클릭 이벤트 처리
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
		bt3.setOnClickListener(this);
		bt4.setOnClickListener(this);
		bt5.setOnClickListener(this);
		
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// 클릭 될 버튼2개의 id값을 스위치구문으로 분기
		switch (v.getId()) {

		// 첫번쨰버튼 눌렷을 때
		case R.id.button2:
			
			//페이지 이동 구문
			Intent intetn1 = new Intent(this, NoteList.class);
			startActivity(intetn1);

			break;
			
		case R.id.button3:
			
			Intent intetn2 = new Intent(this, InfoMain.class);
			startActivity(intetn2);
			
			break;


		}

	}
}