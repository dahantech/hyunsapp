package com.example.note;


import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class SetMain extends Activity implements OnClickListener {
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.setting_main);
        
        Button bt1 = (Button) findViewById(R.id.button1);
		

		// 버튼 클릭 이벤트 처리
		bt1.setOnClickListener(this);
    }

	@Override
	public void onClick(View v) {
		
		switch (v.getId()) {

		// 첫번쨰버튼 눌렷을 때
		case R.id.button1:
			
			//페이지 이동 구문
			Intent intetn1 = new Intent(this, MainActivity.class);
			startActivity(intetn1);

			break;
			
		// TODO Auto-generated method stub
		
		}
	}
}