package com.example.note;

import java.util.ArrayList;
import java.util.List;


import android.os.Bundle;
import android.util.Log;
import android.app.ListActivity;
import android.content.Intent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

public class ViewNotes extends ListActivity implements OnClickListener {

	ArrayList<String> listAdapter = new ArrayList<String>();
	ArrayList<Note> CustomMemoList;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_notes2);
		CustomMemoList = new ArrayList<Note>();
		
		Button bt1 = (Button) findViewById(R.id.notesel1);
		Button bt2 = (Button) findViewById(R.id.notesel2);
		
		bt1.setOnClickListener(this);
		bt2.setOnClickListener(this);
		
		NoteDBAdapter db = new NoteDBAdapter(this);
		
		NoteHelper.noteList = db.getAllNotes();
		int size = NoteHelper.noteList.size();
		Note note;
		
		for ( int i = size - 1 ; i >= 0; i-- ) {
			
			note = NoteHelper.noteList.get(i);
			//String entry = Integer.toString(note.getDBID()) + ": " + note.getTitle();
			CustomMemoList.add(note);
			
		}
		
		// 커스텀 어댑터를 적용
		ListView listView = (ListView)findViewById(android.R.id.list);
		NoteAdapter customAdapter = new NoteAdapter(this, CustomMemoList);
		listView.setAdapter(customAdapter);
		
		// 기본 리스트 뷰를 적용하는 경우는 주석 처리함
		/*for ( int i = size - 1 ; i >= 0; i-- ) {
			
			note = NoteHelper.noteList.get(i);
			String entry = Integer.toString(note.getDBID()) + ": " + note.getTitle();
			listAdapter.add(entry);
			
		}
		setListAdapter(new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listAdapter));*/
		
		listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
	            @Override
	            public void onItemClick(AdapterView<?> parent, View v, int position, long id) {
	            	Intent intent = new Intent( ViewNotes.this, DetailedNote.class );
	            	int a = CustomMemoList.get( position ).getDBID();
	            	String b = Integer.toString(a);
	        		intent.putExtra("key", b );
	        		startActivity(intent);
	            	Log.i("tag", b);
	            	
	            }
        }) ;
	}


	
	//기본 리스트뷰의 리스트 이벤트는 주석 처리
	//@Override
	//public void onListItemClick(ListView parent, View v, int position, long id)  {
		
		/*String key = getListAdapter().getItem(position).toString();
		String[] str = key.split(": ");
		String entryId = str[0];
		
		Intent myIntent = new Intent(ViewNotes.this, DetailedNote.class);
		myIntent.putExtra("key", entryId);

		startActivity(myIntent);*/
		
		
	//}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		// 클릭 될 버튼2개의 id값을 스위치구문으로 분기
		switch (v.getId()) {

		// 첫번쨰버튼 눌렷을 때
		case R.id.notesel1:
			
			
			
			Intent intent1 = new Intent(this, ViewNotes.class);
			
			startActivity(intent1);

			break;
			
		case R.id.notesel2:
			
			//페이지 이동 구문
			Bundle b = new Bundle();
			b.putString("activity", "main");
			b.putInt("id", 0);
			
			Intent intent2 = new Intent(this, AddNote.class);
			intent2.putExtras(b);
			startActivity(intent2);

			break;
			


		}

	}


}
